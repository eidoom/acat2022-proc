# [acat2022-proc](https://gitlab.com/eidoom/acat2022-proc)

* [Live here](https://eidoom.gitlab.io/acat2022-proc/proc.pdf)
* Proceedings for [ACAT 2022 talk](https://gitlab.com/eidoom/acat2022-talk)

## Licence

* Source code: GPLv3 (see LICENSE)
* Content, including figures and compiled document: CC BY 4.0 (see CC-BY-4.0)
