#!/usr/bin/env bash

file=proc.tex

./gen_bib.bash
latexmk -pdf $file

base=$(basename $file .tex)
xdg-open $base.pdf

ls * | entr -s "./gen_bib.bash && latexmk -pdf $file"
