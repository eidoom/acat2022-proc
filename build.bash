#!/usr/bin/env bash

file=proc.tex
base=$(basename $file .tex)

pdflatex $file
bibtex $base
pdflatex $file
pdflatex $file
